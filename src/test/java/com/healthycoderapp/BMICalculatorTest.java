package com.healthycoderapp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BMICalculatorTest {

    @ParameterizedTest
    @ValueSource(doubles = {40,50,60,70})
    void parameterised_test(Double coderWeight) {
        //Given
        Double height = 1.80;
        Double weight = coderWeight;

        //When
        Boolean result = BMICalculator.isDietRecommended(weight, height);

        //then
        assertFalse(result);
    }

    @ParameterizedTest
    @CsvSource( value = {"1.80,70" ,"1.70, 70" })
    void parameterised_csv_test(Double coderWeight, Double coderHeight) {
        //Given
        Double height = coderHeight;
        Double weight = coderWeight;

        //When
        Boolean result = BMICalculator.isDietRecommended(weight, height);

        //then
        assertFalse(result);
    }

    @Test
    void Test () {
        //Given
        Double height = 1.80;
        Double weight = 60.00;

        //When
        Boolean result = BMICalculator.isDietRecommended(weight, height);

        //then
        assertFalse(result);
    }

    @Test
    void should_Throw_Exception_when_height_zero() {
        //Given
        Double height = 0.00;
        Double weight = 60.00;

        //When
        Executable executable = () -> BMICalculator.isDietRecommended(weight, height);

        //then
        assertThrows(ArithmeticException.class, executable);
    }

    @Test
    void should_return_coder_with_worst_BMI() {
        //Given
        List<Coder> coders = new ArrayList<>();
        coders.add(new Coder(1.85,66));
        coders.add(new Coder(1.55,89));
        coders.add(new Coder(1.84,84));


        //When
        Coder result = BMICalculator.findCoderWithWorstBMI(coders);

        //then
        assertAll(
                () -> assertEquals(1.55, result.getHeight()),
                () -> assertEquals(89, result.getWeight())
        );

    }

}